# https://www.terraform.io/docs/providers/aws/r/ses_domain_identity.html

resource "aws_ses_domain_identity" "gnar" {
  domain = "${var.domain}"
}

# https://www.terraform.io/docs/providers/aws/r/ses_domain_dkim.html

resource "aws_ses_domain_dkim" "gnar" {
  domain = "${aws_ses_domain_identity.gnar.domain}"
}

resource "aws_route53_record" "gnar_amazonses_verification_record_dkim" {
  count   = 3
  zone_id = "${aws_route53_zone.root.zone_id}"
  name    = "${element(aws_ses_domain_dkim.gnar.dkim_tokens, count.index)}._domainkey.${var.domain}"
  type    = "CNAME"
  ttl     = "600"
  records = ["${element(aws_ses_domain_dkim.gnar.dkim_tokens, count.index)}.dkim.amazonses.com"]
}

# https://www.terraform.io/docs/providers/aws/r/ses_domain_identity.html
resource "aws_route53_record" "gnar_amazonses_verification_record" {
  zone_id = "${aws_route53_zone.root.zone_id}"
  name    = "_amazonses"
  type    = "TXT"
  ttl     = "600"
  records = ["${aws_ses_domain_identity.gnar.verification_token}"]
}
